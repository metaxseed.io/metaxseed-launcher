import './Home.css';
import { ConfigService } from './service/config-service';
export default () => {
  function getGames() {
    console.log(ConfigService.config);
    return (ConfigService.config as any)?.games.map((item: any) => {
      return (
        <div className="game-card aod">
          <span>{item.name}</span>
          <button
            className="game-card button"
            onClick={() => {
              window.electron.downloadGame(item.downloadPath);
            }}
          >
            download
          </button>
        </div>
      );
    });
  }
  return (
    <div className="home-container">
      <div className="nav">Games</div>
      <div className="game-list">
        {getGames()}
        <div className="game-card aod">
          <span>Age of Dragons</span>
        </div>
        <div className="game-card bfm">
          <span>Block Football</span>
        </div>
        <div className="game-card cski">
          <span>Chainski</span>
        </div>
      </div>
    </div>
  );
};
