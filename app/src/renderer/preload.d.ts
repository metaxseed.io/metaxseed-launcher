import { Channels } from 'main/preload';

declare global {
  interface Window {
    api: {
      send: (channel: any, data: any) => void;
      receive: (channel: any, func: any) => void;
    };
    electron: {
      ipcRenderer: {
        sendMessage(channel: Channels, args: unknown[]): void;
        on(
          channel: string,
          func: (...args: unknown[]) => void
        ): (() => void) | undefined;
        once(channel: string, func: (...args: unknown[]) => void): void;
      };
      getConfig: () => void;
      downloadGame: (url: string) => void;
    };
  }
}

export {};
