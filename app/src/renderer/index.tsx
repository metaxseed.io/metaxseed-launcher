import { createRoot } from 'react-dom/client';
import App from './App';
(window as any).eventBus = {
  subscribers: [],
  subscribe: (callback: any) => {
    (window as any).eventBus.subscribers.push(callback);
  },
  dispatch: (eventName: string, data: any) => {
    (window as any).eventBus.subscribers.forEach((callback: any) => {
      callback(eventName, data);
    });
  },
};
const container = document.getElementById('root')!;
const root = createRoot(container);
root.render(<App />);

// calling IPC exposed from preload script
window.electron.ipcRenderer.once('ipc-example', (arg) => {
  // eslint-disable-next-line no-console
  console.log(arg);
});
window.electron.ipcRenderer.sendMessage('ipc-example', ['ping']);
window.electron.getConfig();
