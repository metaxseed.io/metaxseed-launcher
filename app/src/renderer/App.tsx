import React, { useState } from 'react';
import {
  MemoryRouter as Router,
  Routes,
  Route,
  useNavigate,
} from 'react-router-dom';
import icon from '../../assets/games.svg';
import playButton from '../../assets/play-button.svg';
import './App.css';
import Home from './Home';
import { ConfigService } from './service/config-service';

const Start = () => {
  const [config, setConfig] = useState();
  (window as any).electron.api.receive('onConfig', (data) => {
    console.log(`Received ${JSON.parse(data)} from main process`);
    ConfigService.setConfig(JSON.parse(data));
    setConfig(JSON.parse(data));
  });
  const navigate = useNavigate();
  const onClick = () => {
    navigate('/home');
  };
  return (
    <div>
      <div className="start">
        <img className="start-logo" alt="icon" src={icon} />
        {config ? (
          <>
            <button type="button" className="start-button" onClick={onClick}>
              <img className="play-button" alt="play" src={playButton} />
            </button>

            <span className="start-button-text">press button to start</span>
          </>
        ) : (
          <span className="">loading</span>
        )}
      </div>
    </div>
  );
};

export default function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Start />} />
        <Route path="/home" element={<Home />} />
      </Routes>
    </Router>
  );
}
